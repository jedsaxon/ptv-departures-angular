# PtvDeparturesV2

This project tracks all Public Transport Victoria (PTV) routes in Australia, Vicotria using Angular. I would not recomend using this project as it is not very efficient in the way it handles API requests - for every route that you visit, there will be around 1-2 API requests submitted. I do plan on turning this project into a native Android application the current app and I will post any information about it in this `README.md` once I start. If you do want to use the PTV API, this project does contain a very good resource in `/src/app/services/ptv-api` which contains the code which performs the API requests which you can feel free to use in your applications.

The application does work though, and can view all public transport routes in real-time. 

Obviously, all transport data has been gathered from the [Public Transport Victoria API](https://timetableapi.ptv.vic.gov.au/swagger/ui/index). You will be required to set the environment variable `devId` which you can get through that link.

## Development

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
