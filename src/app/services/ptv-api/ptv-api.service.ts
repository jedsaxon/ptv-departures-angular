import { EventEmitter, Injectable } from '@angular/core';
import { environment } from "../../../environments/environment";
import * as CryptoJS from 'crypto-js';
import { HttpClient } from "@angular/common/http";
import {Observable, lastValueFrom} from "rxjs";
import {ApiErrorResponse} from "../../models/api-responses/api-error-response-model";
import { PtStopModel } from 'src/app/models/pt-stop-model';
import { PtRouteTypeModel } from 'src/app/models/pt-route-type-model';
import { LocationModel } from 'src/app/models/location-model';
import { PtRouteModel } from 'src/app/models/pt-route-model';
import { StopSearchResultsComponent } from 'src/app/components/search-components/stop-search-results/stop-search-results.component';
import { StopSearchResultModel } from 'src/app/models/api-responses/stop-search-result-model';

@Injectable({
  providedIn: 'root'
})
/**
 * The service that communicates with the PTV API
 */
export class PtvApiService {
  // --- START/STOP SEARCH EVENT ---
  public startStopSearch: EventEmitter<void> = new EventEmitter<void>();
  public finishedStopSearch: EventEmitter<StopSearchResultModel[] | ApiErrorResponse> = new EventEmitter<StopSearchResultModel[] | ApiErrorResponse>();

  private _httpClient: HttpClient;

  constructor(httpClient: HttpClient) {
    this._httpClient = httpClient;
  }

  /**
   * Returns a HTTP request that contains the directions for a spceified route. For example, a train may
   * go towards Frankston and City (Flinders Street)
   * @param routeId
   * @constructor
   */
  public GetDirectionsForRoute(routeId: number) {
    const url = this.constructUrl(`/v3/directions/route/${routeId}`);
    return this._httpClient.get<any>(url);
  }

  public GetDirectionInformation(directionId: number) {
    const url = this.constructUrl(`/v3/directions/${directionId}`);
    return this._httpClient.get<any>(url);
  }

  /**
   * Returns a HTTP request that contains data about a route
   * @param routeId
   * @constructor
   */
  public GetRoute(routeId: number) {
    const url = this.constructUrl(`/v3/routes/${routeId}`);
    return this._httpClient.get<any>(url);
  }

  /**
   * Gives information about a given stop
   * @param stopId The stop's ID
   * @param routeType the route type, such as train or bus
   * @constructor
   */
  public GetStopData(stopId: number, routeType: number) {
    const url = this.constructUrl(`/v3/stops/${stopId}/route_type/${routeType}?stop_location=true&stop_disruptions=true`);
    return this._httpClient.get<any>(url);
  }

  /**
   * Gives a list of PT options, such as trains, or buses
   * @constructor
   */
  public GetRouteTypes(): Observable<any> {
    const url = this.constructUrl("/v3/route_types");
    return this._httpClient.get<any>(url);
  }

  /**
   * Returns a HTTP request that contains data about departures for a given route on a stop and direction.
   * @param routeType Train, tram, bus, etc...
   * @param stopId
   * @param routeId
   * @param directionId
   * @constructor
   */
  public GetDeparturesForRoute(routeType: number, stopId: number, routeId: number, directionId: number) {
    const requestUrl = this.constructUrl(`/v3/departures/route_type/${routeType}/stop/${stopId}/route/${routeId}?direction_id=${directionId}&max_results=15`);
    return this._httpClient.get<any>(requestUrl);
  }

  /**
   * Returns all the stops along a route
   * @param routeId
   * @param routeType
   * @constructor
   */
  public GetStopsForRoute(routeId: number, routeType: number) {
    const requestUrl = this.constructUrl(`/v3/stops/route/${routeId}/route_type/${routeType}`);
    return this._httpClient.get<any>(requestUrl);
  }

  /**
   * Searches for stops given a search term
   * @param routeType The type of PT, such as a train or bus
   * @param searchTerm What you want to search for
   */
  public async SearchForStops(routeTypes: PtRouteTypeModel[], searchTerm: string): Promise<void> {
    this.startStopSearch.emit();

    let htmlifiedQuery = this.htmlify(searchTerm);
    let htmlifiedRouteTypes = this.formatRouteTypes(routeTypes);
    const requestUrl = this.constructUrl(`/v3/search/${htmlifiedQuery}?${htmlifiedRouteTypes}&include_outlets=false`);

    let response;
    try {
      let request$ = this._httpClient.get<any>(requestUrl);
      response = await lastValueFrom(request$);
    } catch (e) {
      if (e instanceof(Error)) {
        this.finishedStopSearch.emit(new ApiErrorResponse(e.message, null));
      } else {
        this.finishedStopSearch.emit(new ApiErrorResponse("Unknown Error Occured", null));
      }
    }
    
    let stops: StopSearchResultModel[] = [];
    for (let i: number = 0; i < response.stops.length; i++) {
      let stop = response.stops[i];
    
      let stopLocation = new LocationModel(stop.stop_latitude, stop.stop_longitude);
      let stopRoutes: PtRouteModel[] = [ ]; // TODO - fill in routes on the search stops request; it's already in the API

      let stopModel = new PtStopModel(stopLocation, stopRoutes, stop.stop_id, stop.stop_name, stop.stop_suburb);
      stops.push(new StopSearchResultModel(stopModel, stop.route_type));
    }

    this.finishedStopSearch.emit(stops);
  }

  // TODO - Unit test
  /**
   * Converts a string into something that can be passed into a html string
   * @param str The string you want to search-components fdor
   * @private
   */
  private htmlify(str: string) {
    let newStr = str
      .trim()
      .replace(/\\/g, '');

    newStr = encodeURIComponent(newStr);

    return newStr;
  }

  // TODO - Unit Test
  /**
   * api MUST NOT INCLUDE https://[...]. An example of something valid would be /controller/action/5
   * something that is NOT valid would be: https://example.com/controller/action/5
   */
  private constructUrl(api: string): string {
    let newUrl;
    if (api.includes('?')) {
      newUrl = `${api}&devid=${environment.devId}`;
    } else {
      newUrl = `${api}?devid=${environment.devId}`;
    }

    const signature = CryptoJS.HmacSHA1(newUrl, environment.devKey);
    return `https://timetableapi.ptv.vic.gov.au${newUrl}&signature=${signature}`;
  }

  // TODO - Unit Test
  /**
   * Formats route types to be used in the PTV API. They must be separated by a new line
   * @param routeTypes The route types to format
   * @returns a string that contains each route type's id on a new line
   */
  private formatRouteTypes(routeTypes: PtRouteTypeModel[]): string {
    let formatted = "";
    for (let i = 0; i < routeTypes.length; i++) {
      formatted += "route_types=" + routeTypes[i].Id.toString() + "&";
    }
    return formatted.substring(0, formatted.length - 1); // remove last '&'
  }
}
