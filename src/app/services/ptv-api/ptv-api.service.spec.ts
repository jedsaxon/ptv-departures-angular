import { TestBed } from '@angular/core/testing';

import { PtvApiService } from './ptv-api.service';

describe('PtvApiService', () => {
  let service: PtvApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PtvApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
