import { TestBed } from '@angular/core/testing';

import { PtRouteService } from './pt-route.service';

describe('PtRouteService', () => {
  let service: PtRouteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PtRouteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
