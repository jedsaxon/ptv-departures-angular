import {EventEmitter, Injectable} from '@angular/core';
import {PtvApiService} from "../ptv-api/ptv-api.service";
import {PtRouteModel} from "../../models/pt-route-model";
import {lastValueFrom} from "rxjs";
import {PtDirection} from "../../models/pt-direction";
import {PtDeparture} from "../../models/pt-departure";

@Injectable({
  providedIn: 'root'
})
export class PtRouteService {
  public newRouteSelected: EventEmitter<PtRouteModel | null> = new EventEmitter<PtRouteModel | null>();
  public newDirectionSelected: EventEmitter<PtDirection | null> = new EventEmitter<PtDirection | null>();

  private _ptvApi: PtvApiService;

  constructor(ptvApi: PtvApiService) {
    this._ptvApi = ptvApi;
  }

  /**
   * Sends a HTTP request to recieve a route based on a specified route id
   * @param routeId The route to serach for
   * @constructor
   */
  public async SelectNewRoute(routeId: number): Promise<void> {
    let results;

    try {
      const request$ = this._ptvApi.GetRoute(routeId);
      results = await lastValueFrom(request$);
    } catch (e) {
      console.error(e);
      return;
    }

    const route: PtRouteModel = new PtRouteModel(results.route.route_name, results.route.route_number, results.route.route_type, results.route.route_id);
    this.newRouteSelected.emit(route);
  }

  /**
   * Returns a list of all departures for a given route at a stop and direction. For example, you may want to find the departures
   * for the Sandringham line at South Yarra station going in the City Loop direction
   * @param routeType The route type, i.e. train (0), tram (1), bus (2), etc
   * @param stopId The stop to list departures for
   * @param routeId The route at the stop to search for
   * @param directionId The direction which the train/tram/etc is going towards
   * @constructor
   */
  public async GetDeparturesForStop(routeType: number, stopId: number, routeId: number, directionId: number) {


    let departureResults;

    try {
      const request$ = this._ptvApi.GetDeparturesForRoute(routeType, stopId, routeId, directionId);
      departureResults = await lastValueFrom(request$);
    } catch (e) {
      console.error(e);
      return null;
    }

    let departures: PtDeparture[] = [ ]

    for (let i: number = 0; i < departureResults.departures.length; i++) {
      let departure = departureResults.departures[i];
      departures.push(new PtDeparture(new Date(departure.scheduled_departure_utc)));
    }

    return departures;
  }

  /**
   * Lists directions on a specified route. This is not from point A to point B, but instead the location that
   * the train/tram/etc will go towards
   * @param routeId
   * @constructor
   */
  public async GetDirectionsForRoute(routeId: number): Promise<PtDirection[] | null> {
    let results;

    try {
      const request$ = this._ptvApi.GetDirectionsForRoute(routeId);
      results = await lastValueFrom(request$);
    } catch (e) {
      console.error(e);
      return null;
    }

    let directions: PtDirection[] = [ ];
    for (let i: number = 0; i < results.directions.length; i++) {
      let direction = results.directions[i];
      directions.push(new PtDirection(direction.direction_name, direction.direction_id));
    }

    return directions;
  }
}
