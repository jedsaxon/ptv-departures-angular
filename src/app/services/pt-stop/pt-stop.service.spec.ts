import { TestBed } from '@angular/core/testing';

import { PtStopService } from './pt-stop.service';

describe('SelectedPtStopStoreService', () => {
  let service: PtStopService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PtStopService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
