import {EventEmitter, Injectable, OnInit} from '@angular/core';
import { PtStopModel } from "../../models/pt-stop-model";
import {PtvApiService} from "../ptv-api/ptv-api.service";
import {lastValueFrom} from "rxjs";
import {LocationModel} from "../../models/location-model";
import {PtRouteModel} from "../../models/pt-route-model";
import {PtDeparture} from "../../models/pt-departure";
import {PtRouteTypeModel} from "../../models/pt-route-type-model";
import {PtDirection} from "../../models/pt-direction";

@Injectable({
  providedIn: 'root'
})
export class PtStopService {
  // Stops
  private _stop: PtStopModel | null = null;
  public searchingForNewStop: EventEmitter<any> = new EventEmitter();
  public newStopSelected: EventEmitter<PtStopModel | null> = new EventEmitter<PtStopModel | null>();

  private _ptvApi: PtvApiService;

  constructor (ptvApi: PtvApiService) {
    this._ptvApi = ptvApi;
  }

  /**
   * Selects a new stop
   * @param id The id for the stop
   * @param routeType The route type, i.e. train (0), tram (1), bus (2), etc
   * @constructor
   */
  public async SelectNewStop(id: number, routeType: number) {
    this.searchingForNewStop.emit();
    let results;

    // Create request
    try {
      const request$ = this._ptvApi.GetStopData(id, routeType);
      results = await lastValueFrom(request$);
    } catch (e) {
      console.error(e);
      return;
    }

    // Get location and routes
    const location: LocationModel = this.ConstructLocation(results);
    const routes: PtRouteModel[] = this.ConstructRoutes(results);

    // Create the stop
    this.Stop = new PtStopModel(location, routes, results.stop.stop_id, results.stop.stop_name, results.stop.stop_location.suburb);
  }

  /**
   * Gets all stops for a specified route
   * @param routeId The route
   * @param routeType The route type, i.e. train (0), tram (1), bus (2), etc
   * @constructor
   */
  public async GetStopsForRoute(routeId: number, routeType: number) {
    let results;

    // Create request
    try {
      const request$ = this._ptvApi.GetStopsForRoute(routeId, routeType);
      results = await lastValueFrom(request$);
    } catch (e) {
      console.error(e);
      return null;
    }

    // Extract data from response and put it into an array of stops
    let stops: PtStopModel[] = [ ]
    for (let i: number = 0;  i < results.stops.length; i++) {
      let stopData = results.stops[i];
      let stop: PtStopModel = new PtStopModel(
        new LocationModel(stopData.stop_latitude, stopData.stop_longitude),
        [ ], // todo - please kill me
        stopData.stop_id,
        stopData.stop_name,
        stopData.stop_suburb
      );
      stops.push(stop);
    }

    return stops;
  }

  /**
   * Simple helper function that gets gps data from the HTTP response
   * and turns it into a location model for simplicity
   * @param requestResults The response that contains stop_location.gps
   * @constructor
   * @private
   */
  private ConstructLocation(requestResults: any): LocationModel {
    let latitude = requestResults.stop.stop_location.gps.latitude;
    let longitude = requestResults.stop.stop_location.gps.longitude;

    return new LocationModel(latitude, longitude);
  }

  /**
   * Simple helper method that extracts route data from the request
   * @param requestResults
   * @constructor
   * @private
   */
  private ConstructRoutes(requestResults: any): PtRouteModel[] {
    let routes: PtRouteModel[] = [ ];
    for (let i: number = 0; i < requestResults.stop.routes.length; i++) {
      let dataRoute = requestResults.stop.routes[i];
      let newRoute = new PtRouteModel(dataRoute.route_name, dataRoute.route_number, dataRoute.route_type, dataRoute.route_id);
      routes.push(newRoute);
    }
    return routes;
  }

  private set Stop(value: PtStopModel) {
    this.newStopSelected.emit(value);
    this._stop = value;
  }
}
