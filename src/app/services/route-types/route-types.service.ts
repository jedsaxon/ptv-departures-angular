import { Injectable } from '@angular/core';
import { PtvApiService } from '../ptv-api/ptv-api.service';
import { lastValueFrom } from 'rxjs';
import { PtRouteModel } from 'src/app/models/pt-route-model';
import { PtRouteTypeModel } from 'src/app/models/pt-route-type-model';

@Injectable({
  providedIn: 'root'
})
export class RouteTypesService {
  private _ptvApiService: PtvApiService;

  constructor(ptvApiService : PtvApiService) { 
    this._ptvApiService = ptvApiService;
  }

  public async GetRouteTypes(): Promise<PtRouteTypeModel[]> {
    const request$ = this._ptvApiService.GetRouteTypes();
    const response = await lastValueFrom(request$);
    
    let routeTypes: PtRouteTypeModel[] = []
    for (let i = 0; i < response.route_types.length; i++) {
      let routeType = response.route_types[i];
      routeTypes.push(new PtRouteTypeModel(routeType.route_type_name, routeType.route_type));
    }

    return routeTypes;
  }
}
