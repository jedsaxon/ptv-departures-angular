export class PtDelay {
  private _name: string
  private _type: string;

  constructor (name: string, type: string) {
    this._name = name;
    this._type = type;
  }

  public get Name() {
    return this._name;
  }

  public get Type() {
    return this._type;
  }
}
