export class LocationModel {
  private _longitude: number;
  private _latitude: number;

  constructor(latitude: number, longitude: number) {
    this._latitude = latitude;
    this._longitude = longitude;
  }

  public get Latitude() {
    return this._latitude;
  }

  public set Latitude(latitude: number) {
    this._latitude = latitude;
  }

  public get Longitude() {
    return this._longitude;
  }

  public set Longitude(longitude: number) {
    this._longitude = longitude;
  }
}
