import {PtStopModel} from "./pt-stop-model";

export class PtRouteModel {
  private _routeName: string;
  private _routeNumber: number | null;
  private _routeType: number;
  private _routeId: number;

  constructor(routeName: string, routeNumber: number | null, routeType: number, routeId: number) {
    this._routeName = routeName;
    this._routeNumber = routeNumber;
    this._routeType = routeType;
    this._routeId = routeId;
  }

  public get RouteName() {
    return this._routeName;
  }

  public get RouteNumber() {
    return this._routeNumber;
  }

  public get RouteType() {
    return this._routeType;
  }

  public get RouteId() {
    return this._routeId;
  }
}
