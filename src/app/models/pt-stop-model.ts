import {PtRouteModel} from "./pt-route-model";
import {LocationModel} from "./location-model";
import {PtDeparture} from "./pt-departure";

export class PtStopModel {
  private _location: LocationModel;
  private _routes: PtRouteModel[];
  private _stopId: number;
  private _stopName: string;
  private _stopSuburb: string;

  constructor(location: LocationModel, routes: PtRouteModel[], stopId: number, stopName: string, stopSuburb: string) {
    this._location = location;
    this._stopId = stopId;
    this._stopName = stopName;
    this._stopSuburb = stopSuburb;
    this._routes = routes;
  }

  public get Location() {
    return this._location;
  }

  public get StopId() {
    return this._stopId;
  }

  public get StopName() {
    return this._stopName;
  }

  public get StopSuburb() {
    return this._stopSuburb;
  }

  public get Routes() {
    return this._routes;
  }
}
