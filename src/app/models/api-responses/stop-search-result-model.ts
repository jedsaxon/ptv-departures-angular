import { PtStopModel } from "../pt-stop-model";

export class StopSearchResultModel {
    private _stop: PtStopModel;
    private _routeType: number;

    constructor (stops: PtStopModel, routeType: number) {
        this._stop = stops;
        this._routeType = routeType;
    }

    public get Stop() {
        return this._stop;
    }

    public get RouteType() {
        return this._routeType;
    }
}