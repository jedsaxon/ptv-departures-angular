export class ApiErrorResponse {
    _errorString: string;
    _errorCode: number | null;

    constructor (errorString: string, errorCode: number | null) {
        this._errorString = errorString;
        this._errorCode = errorCode;
    }
}