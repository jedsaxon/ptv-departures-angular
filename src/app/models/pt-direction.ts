export class PtDirection {
  private _name: string;
  private _id: number;

  constructor (name: string, id: number) {
    this._name = name;
    this._id = id;
  }

  public get Name() {
    return this._name;
  }

  public get Id() {
    return this._id;
  }
}
