import {DatePipe} from "@angular/common";

export class PtDeparture {
  private _departTime: Date;

  constructor (departTime: Date) {
    this._departTime = departTime;
  }

  public get DepartTime(): Date {
    return this._departTime;
  }

  /**
   * Minutes until departure
   */
  public get DepartText() {
    const now = new Date();
    const utcDate1Converted = new Date();
    utcDate1Converted.setUTCHours(now.getUTCHours());
    utcDate1Converted.setUTCMinutes(now.getUTCMinutes());
    utcDate1Converted.setUTCSeconds(now.getUTCSeconds());
    utcDate1Converted.setUTCMilliseconds(now.getUTCMilliseconds());

    // Calculate the time difference in minutes
    const minutes = Math.ceil((this.DepartTime.getTime() - utcDate1Converted.getTime()) / 60000);
    let text = "";

    const normalisedMinutes = minutes < 0
      ? minutes * -1
      : minutes;

    if (normalisedMinutes > 90) {
      text = "More than 1 hour"
    } else if (normalisedMinutes > 60) {
      text = "1 Hour"
    } else if (normalisedMinutes >= 1) {
      text = `${normalisedMinutes} Minutes`;
    } else {
      text = "Less than 1 minute"
    }

    if (minutes < 0) {
      text = text + " ago";
    }

    return text;
  }
}

