import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {PtStopService} from "../../../services/pt-stop/pt-stop.service";

@Component({
  selector: 'app-page-stop-display',
  templateUrl: './page-stop-display.component.html',
  styleUrls: ['./page-stop-display.component.css'],
})
export class PageStopDisplayComponent {
  private _stopName: string = "[...]";
  private _isLoading: boolean = true;

  private _router: ActivatedRoute;
  private _ptStopService: PtStopService;

  constructor(router: ActivatedRoute, selectedPtStopStore: PtStopService) {
    this._router = router;
    this._ptStopService = selectedPtStopStore;

    this._router.params.subscribe(params => {
      let id = params['stopId'];
      if (id === null || id === undefined) {
        // display error
        return;
      }

      let routeType = params['routeType'];
      if (routeType === null || routeType === undefined) {
        return;
      }

      this._ptStopService.SelectNewStop(id, routeType);
    });

    selectedPtStopStore.searchingForNewStop.subscribe(_ => {
      this._isLoading = true;
    });

    selectedPtStopStore.newStopSelected.subscribe(value => {
      this._isLoading = false;
      if (value !== null) {
        this.StopName = value.StopName;
      } else {
        this.StopName = "[...]";
      }
    });
  }

  public get IsLoading() {
    return this._isLoading;
  }

  public get StopName() {
    return this._stopName;
  }

  private set StopName(value: string) {
    this._stopName = value;
  }
}
