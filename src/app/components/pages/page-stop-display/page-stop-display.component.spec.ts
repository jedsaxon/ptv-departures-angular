import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageStopDisplayComponent } from './page-stop-display.component';

describe('PageStopDisplayComponent', () => {
  let component: PageStopDisplayComponent;
  let fixture: ComponentFixture<PageStopDisplayComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PageStopDisplayComponent]
    });
    fixture = TestBed.createComponent(PageStopDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
