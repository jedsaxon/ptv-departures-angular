import { Component, ViewChild } from '@angular/core';
import { ListRouteTypesComponent } from '../../search-components/list-route-types/list-route-types.component';
import { PtRouteTypeModel } from 'src/app/models/pt-route-type-model';
import { PtvApiService } from 'src/app/services/ptv-api/ptv-api.service';
import { isEmpty } from 'rxjs';
import { PtStopModel } from 'src/app/models/pt-stop-model';
import { Router } from '@angular/router';
import { StopSearchResultModel } from 'src/app/models/api-responses/stop-search-result-model';

@Component({
  selector: 'app-search-components-pt-stops-page',
  templateUrl: './page-search-pt-stops.html',
  styleUrls: ['./page-search-pt-stops.component.css']
})
/**
 * The page which holds both the PT Routes and PT Results
 */
export class PageSearchPtStops {
  private _routeTypes: PtRouteTypeModel[] | null = null;
  private _ptvApi: PtvApiService;
  private _error: string | null = null;
  private _router: Router;

  constructor (ptvApi: PtvApiService, router: Router) {
    this._ptvApi = ptvApi;
    this._router = router;
  }

  public ChangeRouteToStopInformation(searchResult: StopSearchResultModel) {
    this._router.navigate(['stop', searchResult.Stop.StopId, 'routeType', searchResult.RouteType]);
  }

  public RouteTypeSelectionChange(routeTypes: PtRouteTypeModel[]) {
    this._routeTypes = routeTypes;
  }

  public SearchButtonPress(query: string) {
    if (this._routeTypes === null || this._routeTypes.length === 0) {
      this.Error = "Please select a route type";
      return;
    } else if (query === null || query.length === 0) { // TODO - Make this a helper function
      this.Error = "Please enter a search term in the text box";
      return;
    } else {
      this.Error = null;
    }

    this._ptvApi.SearchForStops(this._routeTypes, query);
  }

  public get Error(): string | null{
    return this._error;
  }

  private set Error(value: string | null) {
    this._error = value;
  }
}
