import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSearchPtStops } from './page-search-pt-stops.component';

describe('SearchPtRoutesPageComponent', () => {
  let component: PageSearchPtStops;
  let fixture: ComponentFixture<PageSearchPtStops>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PageSearchPtStops]
    });
    fixture = TestBed.createComponent(PageSearchPtStops);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
