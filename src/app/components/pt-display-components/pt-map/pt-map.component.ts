import {Component, OnInit} from '@angular/core';
import * as Leaflet from 'leaflet';
import {PtStopService} from "../../../services/pt-stop/pt-stop.service";
import {PtRouteService} from "../../../services/pt-route/pt-route.service";
import {PtStopModel} from "../../../models/pt-stop-model";
import {PtRouteModel} from "../../../models/pt-route-model";
import {LocationModel} from "../../../models/location-model";
import {Polyline} from "leaflet";

@Component({
  selector: 'app-pt-map',
  templateUrl: './pt-map.component.html',
  styleUrls: ['./pt-map.component.css']
})
export class PtMapComponent implements OnInit {
  private _map: Leaflet.Map | undefined;

  private _markers: Leaflet.Marker[] = [ ];
  private _polyline: Leaflet.Polyline | null = null;

  private _ptStopService: PtStopService;
  private _ptRouteService: PtRouteService;

  private _currentStop: PtStopModel | null = null;
  private _currentRoute: PtRouteModel | null = null;

  constructor (ptStopService: PtStopService, ptRouteService: PtRouteService) {
    this._ptStopService = ptStopService;
    this._ptRouteService = ptRouteService;

    this._ptStopService.newStopSelected.subscribe(value => {
      this._currentStop = value;
      this.AddMarkerToStopLocation();
    });

    this._ptRouteService.newRouteSelected.subscribe(value => {
      this._currentRoute = value;
      this.AddMarkerToStopsAlongRoute().then(r => {});
    });
  }

  ngOnInit(): void {
    if (this._map === undefined) {
      this.InitMap();
    }
  }


  /**
   * Adds a single marker to where the current stop is located
   * @constructor
   * @private
   */
  private AddMarkerToStopLocation() {
    if (this._currentStop !== null) {
      this.Centre(this._currentStop.Location);
      this.AddMarker(this._currentStop.Location)
    }
  }

  /**
   * Adds a marker and will *eventually* draw a line for each stop on the public transit line
   * @constructor
   * @private
   */
  private async AddMarkerToStopsAlongRoute() {
    if (this._currentRoute !== null) {
      const stops = await this._ptStopService.GetStopsForRoute(this._currentRoute.RouteId, this._currentRoute.RouteType);
      if (stops === null) return;

      this.ClearMarkers();
      // Centre camera to the middle of the route using the average calculation (I think it works?)
      let avgLocation: LocationModel = new LocationModel(0, 0);
      let coordinates: any[] = [ ];

      for (let i: number = 0; i < stops.length; i++) {
        // get lat and long
        let stop = stops[i];
        let stopLat = stop.Location.Latitude;
        let stopLong = stop.Location.Longitude;

        // Add all location things to the average so it can be calculated outside the for loop
        // Simpler than just using the coordinates array
        avgLocation.Latitude += stopLat;
        avgLocation.Longitude += stopLong;

        this.AddMarker(stop.Location);

        // Add the cordinates
        coordinates.push([ stopLat, stopLong ]);

        // If there is no previous stop, don't add a marker between it and the current line
        // no -1 index
        if (i === 0) continue;
        Leaflet.polyline([], { color: 'blue', weight: 3, smoothFactor: 1 }).addTo(this._map!);
      }

      // Centre to the middle of the line
      avgLocation.Latitude = avgLocation.Latitude / stops.length;
      avgLocation.Longitude = avgLocation.Longitude / stops.length;
      this.Centre(avgLocation, 10);
    }
  }

  /**
   * Initialises the map on the screen for the div that has the id of 'map'
   * @constructor
   * @private
   */
  private InitMap() {
    this._map = Leaflet.map('map').setView([-37.840935, 144.946457], 13);

    const tiles = Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 3,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this._map);

    this.ClearMarkers();
  }

  /**
   * Removes all markers on the line
   * @constructor
   */
  public ClearMarkers(): void {
    for (let i: number = 0; i < this._markers.length; i++) {
      let marker = this._markers[i];
      marker.remove();
    }

    this._markers = [ ]
  }

  public AddMarker(location: LocationModel): void {
    if (this._map === undefined) {
      throw Error("Please ensure that the map has correctly spawned: cannot add marker to nonexistent map");
    }

    let marker: Leaflet.Marker = Leaflet.marker([ location.Latitude, location.Longitude ]);
    this._markers.push(marker);
    marker.addTo(this._map);
  }


  public ClearLine() {
    this._polyline?.remove();
  }

  /**
   * Centres the screen to the specified location and zoom
   * @param location Where to move to
   * @param zoom What colour the lamp will glow when turned on.
   * @constructor
   */
  public Centre(location: LocationModel, zoom: number = 14) {
    if (this._map === undefined) {
      this.InitMap();
    }
    this._map!.flyTo([location.Latitude, location.Longitude], zoom);
  }
}
