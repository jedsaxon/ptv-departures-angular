import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PtMapComponent } from './pt-map.component';

describe('PtMapComponent', () => {
  let component: PtMapComponent;
  let fixture: ComponentFixture<PtMapComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PtMapComponent]
    });
    fixture = TestBed.createComponent(PtMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
