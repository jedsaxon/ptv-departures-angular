import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PtStopDelaysComponent } from './pt-stop-delays.component';

describe('PtStopDelaysComponent', () => {
  let component: PtStopDelaysComponent;
  let fixture: ComponentFixture<PtStopDelaysComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PtStopDelaysComponent]
    });
    fixture = TestBed.createComponent(PtStopDelaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
