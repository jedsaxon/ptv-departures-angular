import { Component } from '@angular/core';
import {PtDelay} from "../../../models/pt-delay";

@Component({
  selector: 'app-pt-stop-delays',
  templateUrl: './pt-stop-delays.component.html',
  styleUrls: ['./pt-stop-delays.component.css']
})
export class PtStopDelaysComponent {
  private _delays: PtDelay[] = [
    new PtDelay("Buses Replace Trains", "danger"),
    new PtDelay("Probably something bad", "warning"),
    new PtDelay("Nothing bad at all", "primary")
  ];

  public get Delays() {
    return this._delays;
  }
}
