import { Component } from '@angular/core';
import {PtDeparture} from "../../../models/pt-departure";
import {PtDirection} from "../../../models/pt-direction";
import {PtStopService} from "../../../services/pt-stop/pt-stop.service";
import {PtRouteService} from "../../../services/pt-route/pt-route.service";
import {PtRouteModel} from "../../../models/pt-route-model";
import {PtStopModel} from "../../../models/pt-stop-model";

@Component({
  selector: 'app-pt-stop-departures',
  templateUrl: './pt-stop-departures.component.html',
  styleUrls: ['./pt-stop-departures.component.css']
})
export class PtStopDeparturesComponent {
  private _selectedDirection: PtDirection | null = null;
  private _directions: PtDirection[] | null = null;
  private _departures: PtDeparture[] | null = null;
  private _route: PtRouteModel | null = null;
  private _stop: PtStopModel | null = null;

  private _ptStopService: PtStopService;
  private _ptRouteService: PtRouteService;

  constructor (ptStopService: PtStopService, ptRouteService: PtRouteService) {
    this._ptStopService = ptStopService;
    this._ptRouteService = ptRouteService;

    ptRouteService.newRouteSelected.subscribe(async value => {
      if (value === null) return;
      this._route = value;
      this.Directions = await this._ptRouteService.GetDirectionsForRoute(value.RouteId);
      this.SelectedDirection = null;
      this.Departures = null;
    });

    ptStopService.newStopSelected.subscribe(value => {
      this._stop = value;
    })
  }

  private async UpdateDirections(): Promise<void> {
    if (this.SelectedDirection === null || this._route === null || this._stop === null) return;

    this.Departures = await this._ptRouteService.GetDeparturesForStop(this._route.RouteType, this._stop.StopId, this._route.RouteId, this.SelectedDirection.Id);
  }

  public get SelectedDirection() {
    return this._selectedDirection;
  }

  public set SelectedDirection(direction: PtDirection | null) {
    this._selectedDirection = direction;
    this.UpdateDirections();
  }

  public get Departures() {
    return this._departures;
  }

  private set Departures(value) {
    this._departures = value;
  }

  public get Now() {
    return new Date();
  }

  public get Directions() {
    return this._directions;
  }

  private set Directions(value: PtDirection[] | null) {
    this._directions = value;
  }

  protected readonly Date = Date;
}
