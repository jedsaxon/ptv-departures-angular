import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PtStopDeparturesComponent } from './pt-stop-departures.component';

describe('PtStopDeparturesComponent', () => {
  let component: PtStopDeparturesComponent;
  let fixture: ComponentFixture<PtStopDeparturesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PtStopDeparturesComponent]
    });
    fixture = TestBed.createComponent(PtStopDeparturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
