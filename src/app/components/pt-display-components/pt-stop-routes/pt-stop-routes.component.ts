import { Component } from '@angular/core';
import {PtRouteModel} from "../../../models/pt-route-model";
import {PtStopService} from "../../../services/pt-stop/pt-stop.service";
import {PtRouteService} from "../../../services/pt-route/pt-route.service";

@Component({
  selector: 'app-pt-stop-routes',
  templateUrl: './pt-stop-routes.component.html',
  styleUrls: ['./pt-stop-routes.component.css']
})
export class PtStopRoutesComponent {
  private _searchResults: PtRouteModel[] = []
  private _ptStopService: PtStopService;
  private _ptRouteService;

  constructor (ptStopService: PtStopService, ptRouteService: PtRouteService) {
    this._ptStopService = ptStopService;
    this._ptRouteService = ptRouteService;

    ptStopService.newStopSelected.subscribe(value => {
      if (value === null || value === undefined) return;

      this.Routes = value.Routes;
    });
  }

  public SelectRoute(route: PtRouteModel): void {
    // todo - make this change the route instead of selecting a new route from the service!
    this._ptRouteService.SelectNewRoute(route.RouteId);
  }

  public get Routes() {
    return this._searchResults;
  }

  private set Routes(value: PtRouteModel[]) {
    this._searchResults = value;
  }
}
