import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PtStopRoutesComponent } from './pt-stop-routes.component';

describe('PtStopRoutesComponent', () => {
  let component: PtStopRoutesComponent;
  let fixture: ComponentFixture<PtStopRoutesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PtStopRoutesComponent]
    });
    fixture = TestBed.createComponent(PtStopRoutesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
