import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { PtRouteTypeModel } from 'src/app/models/pt-route-type-model';
import { PtvApiService } from 'src/app/services/ptv-api/ptv-api.service';
import { RouteTypesService } from 'src/app/services/route-types/route-types.service';

@Component({
  selector: 'app-list-route-types',
  templateUrl: './list-route-types.component.html',
  styleUrls: ['./list-route-types.component.css']
})
export class ListRouteTypesComponent implements OnInit {
  @Output() routeSelectionChanged: EventEmitter<PtRouteTypeModel[]> = new EventEmitter<PtRouteTypeModel[]>();
  private _selectedRouteTypes: PtRouteTypeModel[] = [];
  private _routeTypes: PtRouteTypeModel[] = [];
  private _routeTypesService: RouteTypesService;
  private _loading: boolean = false;;

  constructor (routeTypesService: RouteTypesService) {
    this._routeTypesService = routeTypesService;
  }

  ngOnInit(): void {
    this.ClearSelectedRouteTypes();
    this.RefreshRoutes().then(value => {});
  }

  async RefreshRoutes() {
    this._loading = true;
    this.RouteTypes = await this._routeTypesService.GetRouteTypes();
    this._loading = false;
  }

  // --- ROUTE TYPES ---
  public get RouteTypes() {
    return this._routeTypes;
  }

  public set RouteTypes(routeTypes: PtRouteTypeModel[]) {
    this._routeTypes = routeTypes;
    this.routeSelectionChanged.emit(this._selectedRouteTypes);
  }

  // --- SELECTED ROUTE TYPES ---
  public get SelectedRouteTypes() {
    return this._selectedRouteTypes;
  }

  public set SelectedRouteTypes(selectedRouteTypes: PtRouteTypeModel[]) {
    this._selectedRouteTypes = selectedRouteTypes;
  }

  public CheckboxChanged(routeType: PtRouteTypeModel, event: any): void {
    if (event.target.checked) {
      this.AddSelectedRouteType(routeType);
    } else {
      this.RemoveSelectedRouteType(routeType);
    }
  }

  public ClearSelectedRouteTypes(): void {
    this._selectedRouteTypes = [ ]
  }

  public AddSelectedRouteType(newRoute: PtRouteTypeModel): void {
    this._selectedRouteTypes.push(newRoute);
    // this.PrintSelectedRotues();
  }

  public RemoveSelectedRouteType(routeToRemove: PtRouteTypeModel): void {
    // 1 - Find Route Type in array. 
    // 2 - If route type found in array, loop through the rest of the array
    //     and move everything in front of the route to remove, leaving the route to
    //     remove at the end of the array. there is 100% a better way to do this :( .
    // 3 - Pop the array to get rid of the route type

    let foundRouteType = false;
    for (let i = 0; i < this._selectedRouteTypes.length; i++) {
      const element = this._selectedRouteTypes[i];
      if (!foundRouteType) {
        // 1
        if (element === routeToRemove) {
          foundRouteType = true;
          this._selectedRouteTypes[i];
        }
      } else {
        // 2
        this._selectedRouteTypes[i - 1] = element;
      }
    }
    // 3
    if (foundRouteType) {
      this._selectedRouteTypes.pop();
    }    

    // this.PrintSelectedRotues();
  }

  private PrintSelectedRotues() {
    console.clear();
    for (let i = 0; i < this._selectedRouteTypes.length; i++) {
      console.log(this._selectedRouteTypes[i].Name);
    }
  }

  // --- LOADING ---
  public get Loading() {
    return this._loading;
  }
}
