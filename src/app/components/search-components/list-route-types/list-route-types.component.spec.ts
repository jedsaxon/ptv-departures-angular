import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRouteTypesComponent } from './list-route-types.component';

describe('ListRouteTypesComponent', () => {
  let component: ListRouteTypesComponent;
  let fixture: ComponentFixture<ListRouteTypesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListRouteTypesComponent]
    });
    fixture = TestBed.createComponent(ListRouteTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
