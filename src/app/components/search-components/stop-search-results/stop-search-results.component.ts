import { Component, EventEmitter, Output } from '@angular/core';
import { ApiErrorResponse } from 'src/app/models/api-responses/api-error-response-model';
import { StopSearchResultModel } from 'src/app/models/api-responses/stop-search-result-model';
import { PtStopModel } from 'src/app/models/pt-stop-model';
import { PtvApiService } from 'src/app/services/ptv-api/ptv-api.service';

@Component({
  selector: 'app-stop-search-results',
  templateUrl: './stop-search-results.component.html',
  styleUrls: ['./stop-search-results.component.css']
})
export class StopSearchResultsComponent {
  @Output() selectedStop: EventEmitter<StopSearchResultModel> = new EventEmitter<StopSearchResultModel>();
  private _searchResults: StopSearchResultModel[] = [ ];
  private _ptvApi: PtvApiService;

  constructor(ptvApi: PtvApiService) {
    this._ptvApi = ptvApi;
    this._ptvApi.finishedStopSearch.subscribe({
      next: (data: ApiErrorResponse | StopSearchResultModel[]) => {
        if (data instanceof ApiErrorResponse) {
          console.error(data._errorString);
        } else {
          this.SearchResults = data as StopSearchResultModel[];
        }
      }
    });
  }

  public SelectStop(stop: StopSearchResultModel) {
    this.selectedStop.emit(stop);
  }

  public get SearchResults(): StopSearchResultModel[] {
    return this._searchResults;
  }

  private set SearchResults(value: StopSearchResultModel[]) {
    this._searchResults = value;
  }
}
