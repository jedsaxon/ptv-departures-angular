import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StopSearchResultsComponent } from './stop-search-results.component';

describe('StopSearchResultsComponent', () => {
  let component: StopSearchResultsComponent;
  let fixture: ComponentFixture<StopSearchResultsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [StopSearchResultsComponent]
    });
    fixture = TestBed.createComponent(StopSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
