import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent {
  @Output() searchButtonPress: EventEmitter<string> = new EventEmitter<string>();
  private _searchQuery: string = "";

  public SearchButtonPress() {
    this.searchButtonPress.emit(this._searchQuery);
  }

  public get SearchQuery() : string {
    return this._searchQuery;
  }

  public set SearchQuery(query: string) {
    this._searchQuery = query;
  }
}
