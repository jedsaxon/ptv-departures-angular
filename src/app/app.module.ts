import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { PageSearchPtStops } from './components/pages/page-search-pt-stops/page-search-pt-stops.component';
import {FormsModule} from "@angular/forms";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {NgOptimizedImage} from "@angular/common";
import {RouterModule, Routes} from "@angular/router";
import { PtMapComponent } from './components/pt-display-components/pt-map/pt-map.component';
import { PtStopRoutesComponent } from './components/pt-display-components/pt-stop-routes/pt-stop-routes.component';
import { PtStopDelaysComponent } from './components/pt-display-components/pt-stop-delays/pt-stop-delays.component';
import { PtStopDeparturesComponent } from './components/pt-display-components/pt-stop-departures/pt-stop-departures.component';
import { PageStopDisplayComponent } from './components/pages/page-stop-display/page-stop-display.component';
import { ListRouteTypesComponent } from './components/search-components/list-route-types/list-route-types.component';
import { SearchBarComponent } from './components/search-components/search-bar/search-bar.component';
import { StopSearchResultsComponent } from './components/search-components/stop-search-results/stop-search-results.component';

const routes: Routes = [
  { path: "", component: PageSearchPtStops },
  { path: "stop/:stopId/routeType/:routeType", component: PageStopDisplayComponent },
]

@NgModule({
  declarations: [
    AppComponent,
    PageSearchPtStops,
    PtMapComponent,
    PtStopRoutesComponent,
    PtStopDelaysComponent,
    PtStopDeparturesComponent,
    PageStopDisplayComponent,
    ListRouteTypesComponent,
    SearchBarComponent,
    StopSearchResultsComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NgOptimizedImage,
    RouterModule.forRoot(routes)
  ],
  bootstrap: [AppComponent],
  exports: [
    RouterModule
  ],
})
export class AppModule { }
